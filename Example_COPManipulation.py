
import numpy as np

from Irreversibility.TimeSeriesGeneration.Logistic import logistic
from Irreversibility.Metrics.BDS import GetPValue as BDSTest
from Irreversibility.Manipulation.COP import GetTransformedTimeSeries as COPManipulation


tsLength = 100


print( '--- Manipulation: increase irreversibility' )

ts = np.random.uniform( 0.0, 1.0, ( tsLength ) )

oldPV, _ = BDSTest( ts )
print( 'Original p-value: %f' % oldPV )

bestTS, bestPV, bestCorr = COPManipulation( ts, BDSTest, \
                                    {}, pSize = 4, \
                                    increase = True, pvThreshold = 0.01 )

print( 'New p-value: %f' % bestPV )
print( 'Correlation: %f' % bestCorr )





print( '--- Manipulation: decrease irreversibility' )

ts = logistic( tsLen = tsLength, skip = 1000, \
               x = np.random.uniform( 0.01, 0.99 ), r = 4.0 )

oldPV, _ = BDSTest( ts )
print( 'Original p-value: %f' % oldPV )

bestTS, bestPV, bestCorr = COPManipulation( ts, BDSTest, \
                                    {}, pSize = 4, \
                                    increase = False, pvThreshold = 0.5 )

print( 'New p-value: %f' % bestPV )
print( 'Correlation: %f' % bestCorr )

