
import numpy as np
import time



# Generate a time series using the geometric Brownian motions with
# stochastic resetting (srGBM) model. By varying the drift, we here
# create time series with different degrees of irreversibility.
# The irreversibility is then evaluated using the Costa index,
# and the evolution of the p-value is plotted against the drift

tsLength = 1000

from Irreversibility.TimeSeriesGeneration.srGBM import srGBM
from Irreversibility.Metrics.CostaIndex import GetPValue as CostaIndex

allResults = []

for k in range( 50 ):

    drift = 0.0001 * k
    timeSeries = srGBM( tsLen = tsLength, drift = drift, \
                        noiseA = 0.01, r = 0.01 )

    p_value, statistic = CostaIndex( timeSeries )
    allResults.append( ( drift, p_value ) )

allResults = np.array( allResults )


import matplotlib.pyplot as plt

plt.plot( allResults[ :, 0 ], allResults[ :, 1 ] )
plt.yscale( 'log' )
plt.xlabel( 'Drift' )
plt.ylabel( '$p$-value' )
plt.show()
