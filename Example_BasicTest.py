
import numpy as np
import time



# Generate a time series using the Logistic map
# (which we know is irreversible),
# and get the p-value and statistic using the BDS test.

tsLength = 1000

from Irreversibility.TimeSeriesGeneration.Logistic import logistic
timeSeries = logistic( tsLen = tsLength, skip = 1000, \
                       x = np.random.uniform( 0.01, 0.99 ), r = 4.0 )

from Irreversibility.Metrics.BDS import GetPValue as BDSTest
p_value, statistic = BDSTest( timeSeries )

print( 'Results according to the BDS test:' )
print( '\t$p$-value: %f' % ( p_value ) )
print( '\tStatistic: %f' % ( statistic ) )

