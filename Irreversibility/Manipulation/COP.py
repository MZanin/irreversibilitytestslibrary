
import numpy as np

from Irreversibility.Metrics.COP import normWindow, tranfTS



def GetTransformedTimeSeries( timeSeries, test, testParams, \
                              increase = True, pSize = 4, \
                              numIterations = 1000, \
                              pvThreshold = 0.001 ):

    if increase:
        bestPV = 1.0
    else:
        bestPV = 0.0
        
    bestCorr = 0.0
    bestTS = []

    for k in range( numIterations ):

        x0 = np.random.uniform( 0.0, 1.0, (pSize) )
        patt = normWindow( x0 )
        w = tranfTS( timeSeries, patt )

        corrc = np.abs( np.corrcoef( [ timeSeries[ :np.size( w ) ], w ] )[ 0, 1 ] )
        p_value, _ = test( w, **testParams )

        if increase and p_value >= pvThreshold: continue
        if ( not increase ) and p_value < pvThreshold: continue
        if corrc > bestCorr:
            bestPV = p_value
            bestCorr = corrc
            bestTS = w

    if type( bestTS ) == list:
        return bestTS, bestPV, bestCorr
    
    if np.corrcoef( [ timeSeries[ :np.size( bestTS ) ], bestTS ] )[ 0, 1 ] < 0.0:
        bestTS = 1.0 - bestTS

    return bestTS, bestPV, bestCorr

