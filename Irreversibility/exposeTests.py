

from Irreversibility.Metrics.BDS import GetPValue as BDSTest
from Irreversibility.Metrics.Casali import GetPValue as CasaliTest
from Irreversibility.Metrics.COP import GetPValue as COPTest
from Irreversibility.Metrics.CostaIndex import GetPValue as CostaTest
from Irreversibility.Metrics.DFK import GetPValue as DFKTest
from Irreversibility.Metrics.Diks import GetPValue as DiksTest
from Irreversibility.Metrics.LocalCC import GetPValue as LocalCCTest
from Irreversibility.Metrics.MSTrends import GetPValue as MSTrendsTest
from Irreversibility.Metrics.PermPatterns import GetPValue as PPTest
from Irreversibility.Metrics.Pomeau import GetPValue as PomeauTest
from Irreversibility.Metrics.Ramsey import GetPValue as RamseyTest
from Irreversibility.Metrics.Skewness import GetPValue as SkewnessTest
from Irreversibility.Metrics.TernaryCoding import GetPValue as TCTest
from Irreversibility.Metrics.TPLength import GetPValue as TPTest
from Irreversibility.Metrics.VisibilityGraph import GetPValue as VGTest
from Irreversibility.Metrics.Zumbach import GetPValue as ZumbachTest

allTests = [
    [ 'BDS', BDSTest ],
    [ 'Casali', CasaliTest ],
    [ 'COP', COPTest ],
    [ 'Costa Index', CostaTest ],
    [ 'DFK', DFKTest ],
    [ 'Diks', DiksTest ],
    [ 'Local CC', LocalCCTest ],
    [ 'MS Trends', MSTrendsTest ],
    [ 'Permutation patterns', PPTest ],
    [ 'Pomeau', PomeauTest ],
    [ 'Ramsey', RamseyTest ],
    [ 'Skewness', SkewnessTest ],
    [ 'Ternary Coding', TCTest ],
    [ 'TP Length', TPTest ],
    [ 'Visibility Graph', VGTest ],
    [ 'Zumbach', ZumbachTest ],
]
