
"""
Visibility Graph test

From:
    Lacasa, L., Nunez, A., Roldán, É., Parrondo, J. M., & Luque, B. (2012).
    Time series irreversibility: a visibility graph approach. The European Physical Journal B, 85(6), 1-11.
"""

import numpy as np
from scipy.stats import epps_singleton_2samp, ks_2samp
from scipy.stats import entropy
from ..optionalNJIT import optional_njit



@optional_njit( cache=True, nogil=True )            
def GetOneDegree( TS, offset, horizon ):
    
    tsLength = TS.shape[0]
    
    tempDegree = 1.0

    if horizon == -1:
        maxDistance = tsLength
    else:
        maxDistance = offset + horizon
        if maxDistance > tsLength:
            maxDistance = tsLength
    
    for k in range( offset + 2, maxDistance ):
        
        subTS = TS[ offset + 1 : k ]
        a = np.max( subTS )
        if a < TS[ offset ] and a < TS[ k ]:
            tempDegree += 1.0
            
    return tempDegree
    


def GetVGDivergence( dD, rD ):
        
    while True:
        if dD[ -1 ] == 0 and rD[ -1 ] == 0:
            dD = dD[ :-1 ]
            rD = rD[ :-1 ]
            continue
        break
    
    dD /= np.sum( dD )
    rD /= np.sum( rD )
    en = entropy( dD + 0.000001, qk = rD + 0.000001 )
    
    return en




@optional_njit( cache=True, nogil=True )            
def GetAllDegrees( TS, horizon ):

    tsLength = TS.shape[0]
    allDegrees = np.zeros( (tsLength - 2) )
    
    for k in range( tsLength - 2 ):        
        allDegrees[k] = GetOneDegree( TS, k, horizon )


    degreeDistr = np.zeros( (tsLength) )
    
    for k in range( tsLength ):    
        degreeDistr[ k ] = np.sum( allDegrees == k )

        
    return allDegrees, degreeDistr




@optional_njit( cache=True, nogil=True )            
def GetAllDegrees_Parallel( TS, horizon ):

    tsLength = TS.shape[0]
    allDegrees = np.zeros( (tsLength - 2) )
    
    for k in range( tsLength - 2 ):        
        allDegrees[k] = GetOneDegree( TS, k, horizon )


    degreeDistr = np.zeros( (tsLength) )
    
    for k in range( tsLength ):    
        degreeDistr[ k ] = np.sum( allDegrees == k )

        
    return allDegrees, degreeDistr




    
def GetPValue( TS, parallel = False, **kwargs ):

    horizon = kwargs.get( 'horizon', -1 )

    if parallel:
        allD, dD = GetAllDegrees_Parallel( TS, horizon )
        allR, rD = GetAllDegrees_Parallel( TS[ ::-1 ], horizon )
    else:
        allD, dD = GetAllDegrees( TS, horizon )
        allR, rD = GetAllDegrees( TS[ ::-1 ], horizon )

    pV = 1.0
    try:
        pV = epps_singleton_2samp( allD,  allR )[1]
    except:
        pass

    if pV == 1.0:
        try:
            pV = ks_2samp( allD,  allR )[1]
        except:
            pass
    
    div = GetVGDivergence( dD, rD )
        
    return pV, div



def GetStatistic( TS, parallel = False, **kwargs ):

    return GetPValue( TS, parallel = parallel, **kwargs )[ 1 ]

