
"""
Ramsey test

From:
    Ramsey, J. B., & Rothman, P. (1996).
    Time irreversibility and business cycle asymmetry. Journal of Money, Credit and Banking, 28(1), 1-21.
"""

import numpy as np
from scipy.stats import ttest_ind
from ..optionalNJIT import optional_njit



@optional_njit( cache=True, nogil=True )            
def __auxFunction( TS, kappa ):
    
    numPoints = TS.shape[0]

    mean1 = np.zeros( ( numPoints - 2 * kappa ) )
    mean2 = np.zeros( ( numPoints - 2 * kappa ) )
    for k in range( kappa, numPoints - kappa ):
        mean1[ k - kappa ] = TS[k] * TS[k] * TS[k - kappa]
        mean2[ k - kappa ] = TS[k] * TS[k - kappa] * TS[k - kappa]

    return mean1, mean2    




def GetPValue( TS, **kwargs ):

    kappa = kwargs.get( 'kappa', 1 )

    TS = TS - np.mean( TS )

    mean1, mean2 = __auxFunction( TS, kappa )
    
    _, pV = ttest_ind( mean1, mean2 )
    
    return pV, np.mean( mean2 ) - np.mean( mean1 )



def GetStatistic( TS, **kwargs ):

    return GetPValue( TS, **kwargs )[ 1 ]

