
"""
Pomeau test

From:
    Pomeau, Y. (1982). Symétrie des fluctuations dans le renversement du temps.
    Journal de Physique, 43(6), 859-867.
"""

import numpy as np
from ..optionalNJIT import optional_njit




@optional_njit( cache=True, nogil=True )            
def _auxGetPValue( TS, tau = 1 ):
    
    psi = 0.0
    tsLen = TS.shape[0]

    for k in range( tsLen - 3 * tau - 1 ):
    
        psi += TS[ k ] * ( TS[ k + 2 * tau ] - TS[ k + tau ] ) * TS[ k + 3 * tau ]
    
    psi /= float( tsLen - 3 * tau - 1 )
    
    return psi
    




def GetPValue( TS, numRnd = 100, **kwargs ):
    
    tau = kwargs.get( 'tau', 1 )

    psi = 0.0
    tsLen = TS.shape[0]

    for k in range( tsLen - 3 * tau - 1 ):
    
        psi += TS[ k ] * ( TS[ k + 2 * tau ] - TS[ k + tau ] ) * TS[ k + 3 * tau ]
    
    psi /= float( tsLen - 3 * tau - 1 )
    
    if numRnd == 0:
        return 1.0, psi
    
    
    rndPsi = []
    for k in range( numRnd ):
        rndPsi.append( _auxGetPValue( np.random.permutation( TS ), tau = tau ) )
    
    pValue = np.sum( np.abs( psi ) < np.abs( np.array( rndPsi ) ) ) / numRnd
    
    return pValue, psi



def GetStatistic( TS, **kwargs ):

    return GetPValue( TS, numRnd = 0, **kwargs )[ 1 ]

