
"""
Ternary coding test

From:
    Cammarota, C., & Rogora, E. (2007). 
    Time reversal, symbolic series and irreversibility of human heartbeat. 
    Chaos, Solitons & Fractals, 32(5), 1649-1654.
"""

import numpy as np
from statsmodels.stats.descriptivestats import sign_test
from ..optionalNJIT import optional_njit


    

@optional_njit( cache=True, nogil=True )             
def __auxCountN( TS, alpha ):
    
    Np = 0
    Nn = 0
    
    TS = TS[ 1: ] - TS[ :-1 ]
    
    for k in range( TS.shape[0] - 2 ):
        if TS[ k ] > alpha and TS[ k + 1 ] > alpha and TS[ k + 2 ] > alpha:
            Np += 1
        if TS[ k ] < -alpha and TS[ k + 1 ] < -alpha and TS[ k + 2 ] < -alpha:
            Nn += 1
        
    return Np - Nn




def GetPValue( timeSeries, **kwargs ):

    segL = kwargs.get( 'segL', 4 )
    alpha = kwargs.get( 'alpha', 10 )

    if segL <= 3:
        return 1.0, 0.0
    
    numSegments = int( np.floor( np.size( timeSeries ) / segL ) )
    if numSegments < 3:
        return 1.0, 0.0
    
    D = timeSeries[ 1: ] - timeSeries[ :-1 ]
    alpha_star = np.percentile( D, alpha )
    
    Nd = []
    for k in range( numSegments ):
        tTS = timeSeries[ ( k * segL ) : ( ( k + 1 ) * segL ) ]
        Nd.append( __auxCountN( tTS, alpha_star ) )
    
    pValue, M = ( 1.0, 0.0 )
    try:
        M, pValue = sign_test( Nd )
    except:
        pass
    
    return pValue, np.abs( M )



def GetStatistic( timeSeries, **kwargs ):

    return GetPValue( timeSeries, **kwargs )[ 1 ]
