
"""
Skewness test

From:
    Koutsoyiannis, D. (2019)
    Time's arrow in stochastic characterization and simulation of atmospheric and hydrological processes.
    Hydrol. Sci. J. (64), 1013-1037.
    
    Vavoulogiannis, S., Iliopoulou, T., Dimitriadis, P., & Koutsoyiannis, D. (2021). 
    Multiscale temporal irreversibility of streamflow and its stochastic modelling. Hydrology, 8(2), 63.
"""

from scipy.stats import skew
from scipy.stats import norm
import numpy as np



def GetPValue( TS, numRndReps = 100, **kwargs ):

    tdTS = TS[ 1: ] - TS[ :-1 ]
    skewness_orig = skew( TS )
    skewness_td = skew( tdTS )
    a = skewness_td / skewness_orig
    
    rndA = A_For_Rnd_Series( TS, numRndReps )
    z_score = ( np.abs( a ) - np.mean( np.abs( rndA ) ) ) / np.std( rndA )
    p_value = norm.sf( z_score )
    
    return p_value, a






def GetStatistic( TS, **kwargs ):

    tdTS = TS[ 1: ] - TS[ :-1 ]
    skewness_orig = skew( TS )
    skewness_td = skew( tdTS )
    a = skewness_td / skewness_orig

    return a




def A_For_Rnd_Series( TS, numRndReps = 100 ):

    a = []
    
    for k in range( numRndReps ):
        tTS = np.random.permutation( np.copy( TS ) )
        tdTS = tTS[ 1: ] - tTS[ :-1 ]
        skewness_orig = skew( tTS )
        skewness_td = skew( tdTS )
        a.append( skewness_td / skewness_orig )
    
    return a


