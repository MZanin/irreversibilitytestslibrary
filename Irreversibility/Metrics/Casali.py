
"""
Casali test

From:
    Casali, K. R., Casali, A. G., Montano, N., Irigoyen, M. C., Macagnan, F., Guzzetti, S., & Porta, A. (2008).
    Multiple testing strategy for the detection of temporal irreversibility in stationary time series.
    Physical Review E—Statistical, Nonlinear, and Soft Matter Physics, 77(6), 066204.
"""

import numpy as np
from scipy.stats import norm
from ..optionalNJIT import optional_njit




@optional_njit( cache=True, nogil=True )            
def CalculateS( TS, m ):

    k = 2. ** (- 0.5 )
    N = TS.shape[ 0 ]

    S_m = 0.
    denom = 0.
    for i in range( 1, N - m ):
        d_mi = k * ( TS[ i ] - TS[ i + m ] )
        S_m += d_mi ** 3.
        denom += d_mi ** 2.

    S_m /= denom ** ( 3. / 2. )

    return S_m



def GetPValue( TS, numRndReps = 100, **kwargs ):

    m = kwargs.get( 'm', 1 )

    S = CalculateS( TS, m = m )
    
    rndS = []
    for k in range( numRndReps ):
        rndS.append( CalculateS( np.random.permutation( TS ), m = m ) )

    z_score = ( np.abs( S ) ) / np.std( rndS )
    p_value = norm.sf( np.abs( z_score ) )
    
    return p_value, S




def GetStatistic( TS, **kwargs ):

    m = kwargs.get( 'm', 1 )
    S = CalculateS( TS, m = m )
    return S


