
"""
BDS test

From:
    Broock, W. A., Scheinkman, J. A., Dechert, W. D., & LeBaron, B. (1996). 
    A test for independence based on the correlation dimension. Econometric reviews, 15(3), 197-235.
"""

import numpy as np
from statsmodels.tsa.stattools import bds



def GetPValue( TS, **kwargs ):

    max_dim = kwargs.get( 'max_dim', 2 )
    distance = kwargs.get( 'distance', 1.5 )
    
    bds_stat, pV = bds( TS, max_dim = max_dim, epsilon = None, distance = distance )

    if pV.size > 1:
        bds_stat = bds_stat[ np.argmin( pV ) ]
        pV = np.min( pV )
    
    return float( pV ), float( bds_stat )



def GetStatistic( TS, **kwargs ):

    bds_stat, pV = bds( TS, **kwargs )

    if pV.size > 1:
        bds_stat = bds_stat[ np.argmin( pV ) ]
    
    return float( bds_stat )

