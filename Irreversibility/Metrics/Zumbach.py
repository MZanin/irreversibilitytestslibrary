
"""
Zumbach test

From:
    Zumbach, G., (2007).
    Time reversal invariance in finance.
    https://arxiv.org/pdf/0708.4022.pdf
"""

import numpy as np
from scipy.stats import skew
from scipy.stats import norm




def calcAllReturns( ts, granularity ):

    ts = np.log2( ts )
    return ts[ granularity : ] - ts[ : (-granularity) ]


def histVolatility( ts, t, deltaT, allRets ):

    ax = []
    for k in range( t - deltaT, t ):
        if k < 0: continue
        if k >= np.size( allRets ): continue
        ax.append( allRets[ k ] ** 2.0 )
    
    return np.mean( ax )


def realVolatility( ts, t, deltaT, allRets, granularity ):

    ax = []
    for k in range( t + granularity, t + deltaT ):
        if k >= np.size( allRets ): continue
        ax.append( allRets[ k ] ** 2.0 )
    
    return np.mean( ax )





def GetPValue( TS, **kwargs ):

    deltaT = kwargs.get( 'deltaT', 20 )
    granularity = kwargs.get( 'granularity', 3 )

    allRets = calcAllReturns( TS, granularity )

    set1 = []
    set2 = []
    for t in range( np.size( TS ) ):
        set1.append( histVolatility( TS, t, deltaT, allRets ) )
        set2.append( realVolatility( TS, t, deltaT, allRets, granularity ) )

    set1 = np.array( set1 )
    set2 = np.array( set2 )
    notValid = np.any( ( np.isnan( set1 ), np.isnan( set2 ) ), axis = 0 )

    set1 = set1[ ~notValid ]
    set2 = set2[ ~notValid ]

    from scipy.stats import ks_2samp
    if np.size( set1 ) == 0 or np.size( set2 ) == 0:
        return 1.0, 0.0
    
    stat, pV = ks_2samp( set1,  set2 )

    return pV, stat



def GetStatistic( TS, **kwargs ):

    return GetPValue( TS, **kwargs )[ 1 ]

