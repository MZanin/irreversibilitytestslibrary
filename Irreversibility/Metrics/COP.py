
"""
Continuous Ordinal Patterns test

From:
    Zanin, M. (2023).
    Continuous ordinal patterns: Creating a bridge between ordinal analysis and deep learning.
    Chaos: An Interdisciplinary Journal of Nonlinear Science 33 (3).
"""

import numpy as np
from ..optionalNJIT import optional_njit
from scipy.stats import ks_2samp



@optional_njit( cache=True, nogil=True )
def normWindow( ts ):
	
	newTS = np.copy( ts )
	newTS -= np.min( newTS )
	newTS /= np.max( newTS )
	newTS = ( newTS - 0.5 ) * 2.0
	return newTS
	
		


@optional_njit( cache=True, nogil=True )            
def tranfTS( ts, patt ):
	
    tsL = ts.shape[0]
    W = np.zeros( ( tsL - patt.shape[0] + 1 ) )
	
    for t in range( tsL - patt.shape[0] + 1 ):
		
        for l in range( patt.shape[0] ):
            
            W[ t ] += np.abs( normWindow( ts[ t : ( t + patt.shape[0] ) ] )[ l ] - patt[ l ] )

        W[ t ] = W[ t ] / patt.shape[0] / 2.0
		
    return W





def evaluateDistance( patt, TS1, TS2 ):
    
    patt = normWindow( patt )
    
    d_0 = np.empty( ( 0, 1 ) )
    d_r = np.empty( ( 0, 1 ) )

    TS = TS1
    w = tranfTS( TS, patt )
    d_0 = w
    
    TS = TS2
    w = tranfTS( TS, patt )
    d_r = w

    allRes = ks_2samp( d_0, d_r )
       
    return allRes[ 1 ], allRes[ 0 ]



def GetPValue( TS, numIters = -1, **kwargs ):

    pSize = kwargs.get( 'pSize', 3 )
     
    if numIters <= 0:
        numIters = int( 10 ** np.sqrt( pSize - 2 ) )

    bestD = np.zeros( ( numIters ) )
    bestS = np.zeros( ( numIters ) )
    rTS = TS[::-1]

    for k in range( numIters ):
        x0 = np.random.uniform( 0.0, 1.0, (pSize) )
        bestD[ k ], bestS[ k ] = evaluateDistance( x0, np.copy( TS ), np.copy( rTS ) )

    return np.min( bestD ), bestS[ np.argmin( bestD ) ]




def GetStatistic( TS, numIters = -1, **kwargs ):

    return GetPValue( TS, numIters = numIters, **kwargs )[ 1 ]

