
"""
Costa index test

From:
    Costa, M.; Goldberger, A.L.; Peng, C.K.
    Broken asymmetry of the human heartbeat: loss of time irreversibility in aging and disease.
    Physical review letters 2005, 95, 198102.
"""

from scipy.stats import skew
from scipy.stats import norm
import numpy as np



def GetPValue( TS, numRndReps = 100, **kwargs ):

    tau = kwargs.get( 'tau', 1 )

    a = GetStatistic( TS, tau = tau )
    
    rndA = A_For_Rnd_Series( TS, tau = tau, numRndReps = numRndReps )
    z_score = ( np.abs( a ) - np.mean( np.abs( rndA ) ) ) / np.std( rndA )
    p_value = norm.sf( z_score )
    
    return p_value, a




def GetStatistic( TS, **kwargs ):
    
    tau = kwargs.get( 'tau', 1 )

    dTS = TS[ tau: ] - TS[ :-tau ]
    a1 = np.sum( dTS > 0 )
    a2 = np.sum( dTS < 0 )
    return np.abs( a1 - a2 ) / ( a1 + a2 )



def A_For_Rnd_Series( TS, tau, numRndReps = 100 ):

    a = []
    
    for k in range( numRndReps ):
        tTS = np.random.permutation( np.copy( TS ) )
        a.append( GetStatistic( tTS, tau = tau ) )
    
    return a


