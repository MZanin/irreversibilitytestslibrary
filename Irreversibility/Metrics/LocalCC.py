
"""
Local Clustering Coefficient test

From:
    Donges, J. F., Donner, R. V., & Kurths, J. (2013).
    Testing time series irreversibility using complex network methods.
    EPL (Europhysics Letters), 102(1), 10004.
"""

import numpy as np
from scipy.stats import ks_2samp
from ..optionalNJIT import optional_njit


    
    
@optional_njit( cache=True, nogil=True )            
def GetOneConnectivity( TS, offset ):
    
    tsLength = TS.shape[0]
    subAM = np.zeros( ( tsLength ) )
    if offset < tsLength - 1:
        subAM[ offset + 1 ] = 1
        
    for k in range( offset + 2, tsLength ):
        
        subTS = TS[ offset + 1 : k ]
        a = np.max( subTS )
        if a < TS[ offset ] and a < TS[ k ]:
            subAM[ k ] = 1
            
            
    return subAM




def GetFullAM( TS ):

    tsLength = np.size( TS, 0 )
    AM = np.zeros( (tsLength, tsLength) )
    
    for n1 in range( tsLength ):    
        AM[ n1, : ] = GetOneConnectivity( TS, n1 )
        
    for n1 in range( tsLength ):
        for n2 in range( n1 + 1, tsLength ):
            AM[ n2, n1 ] = AM[ n1, n2 ]
        
    return AM





@optional_njit( cache=True, nogil=True )            
def GetLocalClusteringCoefficient( AM ):
    
    numNodes = AM.shape[0]
    
    retardedCC = np.zeros( (numNodes) )
    for n1 in range( numNodes ):
        if np.sum( AM[ n1, :n1 ] ) <= 1.0: continue
        for n2 in range( n1 ):
            if AM[ n1, n2 ] == 0: continue
            for n3 in range( n1 ):
                 retardedCC[ n1 ] += AM[ n1, n2 ] * AM[ n2, n3 ] * AM[ n3, n1 ]
    
    advancedCC = np.zeros( (numNodes) )
    for n1 in range( numNodes ):
        if np.sum( AM[ n1, (n1+1): ] ) <= 1.0: continue
        for n2 in range( n1 + 1, numNodes ):
            if AM[ n1, n2 ] == 0: continue
            for n3 in range( n1 + 1, numNodes ):
                 advancedCC[ n1 ] += AM[ n1, n2 ] * AM[ n2, n3 ] * AM[ n3, n1 ]
                
    return retardedCC, advancedCC





    
def GetPValue( TS ):

    AM = GetFullAM( TS )
    retardedCC, advancedCC = GetLocalClusteringCoefficient( AM )
    stat, pV = ks_2samp( retardedCC,  advancedCC )

    return pV, stat



    
def GetStatistic( TS ):

    return GetPValue( TS )[ 1 ]

