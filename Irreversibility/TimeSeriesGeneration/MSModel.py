
"""
Two variants of a synthetic model to generate time series that
are irreversible at specific time scales.

An explanation can be found in:

Zanin, M., & Papo, D. (2024).
Algorithmic Approaches for Assessing Multiscale Irreversibility in Time Series: Review and Comparison.
"""

import numpy as np
from ..optionalNJIT import optional_njit


@optional_njit( cache=True, nogil=True )   
def msmodel( tsLen = 1000, tau = 2, mu = 0.05, randomInit = False ):

    TS = np.zeros( ( tsLen ) )
                                          
    for i in range( 0, tsLen ):

        if np.mod( i, tau ) == 0:
            TS[ i ] = np.random.uniform( 0., 1. )
        else:
            TS[ i ] = np.mod( TS[ i - 1 ] - mu, 1.0 )
    
    return TS


@optional_njit( cache=True, nogil=True )   
def msmodel_periodic( tsLen = 1000, tau = 2, mu = 0.05, randomInit = False ):

    TS = np.zeros( ( tsLen ) )
                                          
    for i in range( 0, tsLen ):

        if np.mod( i, tau + 1 ) < tau:
            TS[ i ] = np.random.uniform( 0., 1. )
        else:
            TS[ i ] = np.mod( TS[ i - tau ] - mu, 1.0 )
    
    return TS

