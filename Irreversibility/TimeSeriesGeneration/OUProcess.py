
import numpy as np
from ..optionalNJIT import optional_njit


@optional_njit( cache=True, nogil=True )            
def OrnsteinUhlenbeck( tsLen = 1000, x = 0.0, Omega = 0.1, \
                       avg = 0.0, delta = 1.0, randomInit = False ):
      
    if randomInit:
        x = np.random.uniform( 0.0, 1.0 )
        
    TS = np.zeros( (tsLen) )
    TS[0] = x
                                          
    for i in range(1, tsLen):

        x_dot = np.random.normal( 0.0, delta )   
        x_dot = Omega * ( avg - TS[i - 1] ) + x_dot
        TS[i] += x_dot
    
    return TS

