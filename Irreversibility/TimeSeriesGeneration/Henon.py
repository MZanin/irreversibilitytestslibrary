
"""
Simple function to generate a time series using the Henon map.
"""

import numpy as np
from ..optionalNJIT import optional_njit


@optional_njit( cache=True, nogil=True )   
def henon( tsLen = 1000, x = 0.5, y = 0.5, a = 1.4, b = 0.3, randomInit = False ):

    if randomInit:
        x = np.random.uniform( -1., 1. )
        y = np.random.uniform( -0.3, 0.3 )
   
    TS = np.zeros( (tsLen, 2) )
    TS[0, 0] = x
    TS[0, 1] = y
                                          
    for i in range(1, tsLen):

        TS[i, 0] = 1.0 - a * TS[i - 1, 0] * TS[i - 1, 0] + TS[i - 1, 1]
        TS[i, 1] = b * TS[i - 1, 0]
    
    return TS

