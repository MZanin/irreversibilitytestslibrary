
import numpy as np
from ..optionalNJIT import optional_njit


@optional_njit( cache=True, nogil=True )   
def ST( offset, period, w ):

    newOff = np.mod( offset, period ) / period

    refValuesX = [ 0.0, w, 1.0 ]
    refValuesY = [ 0.0, 1.0, 0.0 ]

    value = np.interp( newOff, refValuesX, refValuesY )
    return value



@optional_njit( cache=True, nogil=True )   
def aWeierstrass( tsLen = 1000, period = 100, numHarms = 50, w = 0.2 ):

    TS = np.zeros( ( tsLen ) )

    for k in range( 1, numHarms + 1 ):

        tPeriod = period / k
        tAmpl = 1 / ( k / 2.0 )
        TS += ST( np.arange( tsLen ), tPeriod, w ) * tAmpl

    TS /= np.max( TS )

    return TS


