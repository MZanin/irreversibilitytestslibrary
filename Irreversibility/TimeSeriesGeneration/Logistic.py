
"""
Simple function to generate a time series using the Logistic map.
"""

import numpy as np
from ..optionalNJIT import optional_njit


@optional_njit( cache=True, nogil=True )   
def logistic(tsLen = 1000, skip = 100, x = 0.01, \
             r = 4.0, randomInit = False):
    
    if randomInit:
        x = np.random.uniform( 0.01, 0.99 )

    for t in range( skip ):
        x = r * x * ( 1.0 - x )

    TS = np.zeros( (tsLen) )
                                          
    for t in range( tsLen ):
        x = r * x * ( 1.0 - x )
        TS[t] = x

    return TS
