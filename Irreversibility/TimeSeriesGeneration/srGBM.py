
"""
Function to generate time series using a geometric Brownian motions with stochastic resetting (srGBM). Depending on the parameters, mainly the reset probability and the drift, resulting time series have a tuneable degree of irreversibility.

From:
    Stojkoski, V.; Sandev, T.; Kocarev, L.; Pal, A.
    Geometric Brownian motion under stochastic resetting: A stationary yet nonergodic process.
    Physical Review E 2021, 104, 014121.
"""

import numpy as np
from ..optionalNJIT import optional_njit


@optional_njit( cache=True, nogil=True )   
def srGBM( tsLen = 1000, drift = 0.05,
           noiseA = 0.01, r = 0.01 ):

    TS = np.zeros( ( tsLen ) )
    TS[ 0 ] = 1.
    deltaT = 1.

    for t in range( 1, tsLen ):

        if np.random.uniform( 0., 1. ) < r * deltaT:
            TS[ t ] = 1.0
            continue

        TS[ t ] = TS[ t - 1 ]
        TS[ t ] += TS[ t - 1 ] * ( drift + np.sqrt( deltaT ) * noiseA * np.random.normal( 0.0, 1.0 ) )

    return TS

