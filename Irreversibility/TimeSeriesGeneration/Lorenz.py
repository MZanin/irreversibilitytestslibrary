

import numpy as np
from ..optionalNJIT import optional_njit


@optional_njit( cache=True, nogil=True )   
def lorenz( tsLen = 1000, dT = 0.01, x = 0.0, y = 1.0, z = 1.05, \
            s = 10, r = 28, b = 2.667, randomInit = False ):
      
    if randomInit:
        x = np.random.uniform( -0.5, 0.5 )
        y = np.random.uniform( 0.0, 2.0 )
        z = np.random.uniform( 0.9, 1.1 )

    tsLen = int( tsLen )
    TS = np.zeros( (tsLen, 3) )
    TS[0, 0] = x
    TS[0, 1] = y
    TS[0, 2] = z
                                          
    for i in range(1, tsLen):

        x_dot = s * ( TS[i - 1, 1] - TS[i - 1, 0] )
        y_dot = r * TS[i - 1, 0] - TS[i - 1, 1] - TS[i - 1, 0] * TS[i - 1, 2]
        z_dot = TS[i - 1, 0] * TS[i - 1, 1] - b * TS[i - 1, 2]

        TS[i, 0] = TS[i - 1, 0] + (x_dot * dT)
        TS[i, 1] = TS[i - 1, 1] + (y_dot * dT)
        TS[i, 2] = TS[i - 1, 2] + (z_dot * dT)
    
    return TS

