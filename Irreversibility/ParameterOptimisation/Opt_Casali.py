
import numpy as np
from itertools import product

from Irreversibility.Metrics.Casali import GetPValue


def Optimisation( tsSet ):

    p1Set = np.arange( 1, 101, dtype = int )

    bestPValue = 1.0
    bestParameters = []

    for pSet in p1Set:

        pV = []
        for timeSeries in tsSet:
            pV.append( GetPValue( timeSeries, m = pSet )[ 0 ] )

        if np.median( pV ) < bestPValue:
            bestPValue = np.median( pV )
            bestParameters = { 'm': pSet }

    return bestParameters, bestPValue
