
import numpy as np
from itertools import product

from Irreversibility.Metrics.Pomeau import GetPValue


def Optimisation( tsSet ):

    p1Set = [ 3, 4, 5, 6 ]

    bestPValue = 1.0
    bestParameters = []

    for pSet in product( p1Set ):

        pV = []
        for timeSeries in tsSet:
            pV.append( GetPValue( timeSeries, tau = pSet[ 0 ] )[ 0 ] )

        if np.median( pV ) < bestPValue:
            bestPValue = np.median( pV )
            bestParameters = { 'tau': pSet[ 0 ] }

    return bestParameters, bestPValue
