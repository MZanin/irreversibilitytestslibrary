
import numpy as np
from itertools import product

from Irreversibility.Metrics.CostaIndex import GetPValue


def Optimisation( tsSet ):

    p1Set = [ 1, 2, 3 ]

    bestPValue = 1.0
    bestParameters = []

    for pSet in product( p1Set ):

        pV = []
        for timeSeries in tsSet:
            pV.append( GetPValue( timeSeries, tau = pSet[ 0 ] )[ 0 ] )

        if np.median( pV ) < bestPValue:
            bestPValue = np.median( pV )
            bestParameters = { 'tau': pSet[ 0 ] }

    return bestParameters, bestPValue
