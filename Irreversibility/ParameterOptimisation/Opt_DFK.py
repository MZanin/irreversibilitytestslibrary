
import numpy as np
from itertools import product

from Irreversibility.Metrics.DFK import GetPValue


def Optimisation( tsSet ):

    p1Set = [ 2, 3, 4 ]
    p2Set = [ 2, 3, 4 ]

    bestPValue = 1.0
    bestParameters = []

    for pSet in product( p1Set, p2Set ):

        pV = []
        for timeSeries in tsSet:
            pV.append( GetPValue( timeSeries, n = pSet[ 0 ], L = pSet[ 1 ] )[ 0 ] )

        if np.median( pV ) < bestPValue:
            bestPValue = np.median( pV )
            bestParameters = { 'n': pSet[ 0 ], 'L': pSet[ 1 ] }

    return bestParameters, bestPValue
