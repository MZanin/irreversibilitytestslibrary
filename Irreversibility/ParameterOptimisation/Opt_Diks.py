
import numpy as np
from itertools import product

from Irreversibility.Metrics.Diks import GetPValue


def Optimisation( tsSet ):

    p1Set = [ 4, 6, 8 ]
    p2Set = [ 1.4, 1.6, 1.8 ]

    bestPValue = 1.0
    bestParameters = []

    for pSet in product( p1Set, p2Set ):

        pV = []
        for timeSeries in tsSet:
            pV.append( GetPValue( timeSeries, n = pSet[ 0 ], L = pSet[ 1 ] )[ 0 ] )

        if np.median( pV ) < bestPValue:
            bestPValue = np.median( pV )
            bestParameters = { 'embD': pSet[ 0 ], 'dVar': pSet[ 1 ] }

    return bestParameters, bestPValue
