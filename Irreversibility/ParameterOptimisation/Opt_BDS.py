
import numpy as np
from itertools import product

from Irreversibility.Metrics.BDS import GetPValue


def Optimisation( tsSet ):

    p1Set = [ 2, 3, 4, 5, 6 ]
    p2Set = np.arange( 1.0, 2.01, 0.02 )

    bestPValue = 1.0
    bestParameters = []

    for pSet in product( p1Set, p2Set ):

        pV = []
        for timeSeries in tsSet:
            pV.append( GetPValue( timeSeries, max_dim = pSet[ 0 ], distance = pSet[ 1 ] )[ 0 ] )

        if np.median( pV ) < bestPValue:
            bestPValue = np.median( pV )
            bestParameters = { 'max_dim': pSet[ 0 ], 'distance': pSet[ 1 ] }

    return bestParameters, bestPValue
