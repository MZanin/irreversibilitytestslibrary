
import numpy as np
from itertools import product

from Irreversibility.Metrics.MSTrends import GetPValue


def Optimisation( tsSet ):

    p1Set = [ 2, 3, 4 ]
    p2Set = np.arange( 10, 50.1, 2 )

    bestPValue = 1.0
    bestParameters = []

    for pSet in product( p1Set, p2Set ):

        pV = []
        for timeSeries in tsSet:
            pV.append( GetPValue( timeSeries, wSize = pSet[ 0 ], wSize2 = pSet[ 1 ] )[ 0 ] )

        if np.median( pV ) < bestPValue:
            bestPValue = np.median( pV )
            bestParameters = { 'wSize': pSet[ 0 ], 'wSize2': pSet[ 1 ] }

    return bestParameters, bestPValue
