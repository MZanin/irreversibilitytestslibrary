
import numpy as np
from itertools import product

from Irreversibility.Metrics.TernaryCoding import GetPValue


def Optimisation( tsSet ):

    p1Set = [ 1, 2, 4, 8 ]
    p2Set = [ 5, 10, 20, 40, 80, 160 ]

    bestPValue = 1.0
    bestParameters = []

    for pSet in product( p1Set, p2Set ):

        pV = []
        for timeSeries in tsSet:
            pV.append( GetPValue( timeSeries, wSize = pSet[ 0 ], wSize2 = pSet[ 1 ] )[ 0 ] )

        if np.median( pV ) < bestPValue:
            bestPValue = np.median( pV )
            bestParameters = { 'segL': pSet[ 0 ], 'alpha': pSet[ 1 ] }

    return bestParameters, bestPValue
