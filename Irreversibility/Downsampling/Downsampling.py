
import numpy as np
from scipy.signal import decimate



def OneEveryN( TS, tau ):
    
    return np.copy( TS[ :: tau ] )



def Downsampling_Avg( TS, tau ):
    
    newL = int( np.size( TS ) / tau )
    newTS = np.zeros( ( newL ) )
    for k in range( newL ):
        newTS[ k ] = np.mean( TS[ ( k * tau ) : ( (k+1) * tau ) ] )
    return newTS



def Decimate( TS, tau ):
        
    return decimate( np.copy( TS ), tau )

