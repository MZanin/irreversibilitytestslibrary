
import numpy as np
import time



# Generate a time series using the Logistic map
# (which we know is irreversible),
# and get the p-value using all available tests.

# Note: the computational cost can be quite high,
# so be careful when increasing the size of the
# time series!

tsLength = 100

from Irreversibility.TimeSeriesGeneration.Logistic import logistic
timeSeries = logistic( tsLen = tsLength, randomInit = True )

from Irreversibility.exposeTests import allTests

for test in allTests:
    p_value, _ = test[ 1 ]( timeSeries )
    print( 'Result according to %s: %f' % ( test[ 0 ], p_value ) )

