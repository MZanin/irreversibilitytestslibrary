
import numpy as np



print( '--- Optimisation: Logistic map' )

from Irreversibility.TimeSeriesGeneration.Logistic import logistic

tsLength = 100
tsSet = []
for k in range( 1000 ):
    ts = logistic( tsLen = tsLength, skip = 1000, \
                   x = np.random.uniform( 0.01, 0.99 ), r = 4.0 )
    ts += np.random.uniform( 0.0, 0.5, ( tsLength ) )
    tsSet.append( ts )


from Irreversibility.ParameterOptimisation.Opt_Costa import Optimisation

bestParameters, medianPValue = Optimisation( tsSet )

print( 'Best obtained parameters:' )
print( bestParameters )
print( 'Median $p$-value: %f' % medianPValue )


print( 'Using the best parameters in a new time series:' )

ts = logistic( tsLen = tsLength, skip = 1000, \
                   x = np.random.uniform( 0.01, 0.99 ), r = 4.0 )

from Irreversibility.Metrics.CostaIndex import GetPValue
pValue, stat = GetPValue( ts, **bestParameters )
print( '$p$-value: %f' % pValue )



print( '\n\n--- Optimisation: srGBM' )

from Irreversibility.TimeSeriesGeneration.srGBM import srGBM

tsLength = 100
tsSet = []
for k in range( 1000 ):
    ts = srGBM( tsLen = tsLength, drift = 0.005, \
                        noiseA = 0.01, r = 0.01 )
    ts += np.random.uniform( 0.0, 0.3, ( tsLength ) )
    tsSet.append( ts )


from Irreversibility.ParameterOptimisation.Opt_Costa import Optimisation

bestParameters, medianPValue = Optimisation( tsSet )

print( 'Best obtained parameters:' )
print( bestParameters )
print( 'Median $p$-value: %f' % medianPValue )
